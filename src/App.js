import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div>
      <section className="profile_area">
        <div className="container ">
          <div className="profile">
            <div className="profile_image">
              <img src="https://instagram.fbkk5-3.fna.fbcdn.net/v/t51.2885-19/s150x150/40538509_675390682835877_43972283198341120_n.jpg?_nc_ht=instagram.fbkk5-3.fna.fbcdn.net&_nc_ohc=l-yHhWfpGN8AX9aYObL&tp=1&oh=cef2a5f726bc4ee274a22a769ff3605c&oe=5FF8B0E9" />
            </div>
            <div className="profile_info">
              <div className="profile_info--top">
                <h1>suntiver</h1>
                <a href="#" className="edit"> แก้ไขโปรไฟล์</a>
                <a href="#" className="gear"> <i className="fas fa-cog"></i> </a>
              </div>
              <div className="profile_info--center">
                <span>
                  <strong> 402 </strong> โพสต์
                </span>
                <span>
                  ผู้ติดตาม <strong>146</strong>คน{" "}
                </span>
                <span>
                  กำลังติดตาม<strong>83</strong>คน{" "}
                </span>
              </div>
              <div className="profile_info--bottom">
                <strong>THID</strong>
                <br />
                <p>
                  Web developer & Workoffice web Developer <br />{" "}
                </p>
                เขียนเว็บ | ปรึกษาเรื่อง IOT | 0624547526 <br />
                <a href="https://profile-3642c.web.app/" target="_blank">
                    https://profile-3642c.web.app/
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="tabs_area">
        <div className="container">
          <div className="tabs">
            <div className="tab-item  active">
              <strong>โพสต์</strong>
            </div>
            <div className="tab-item  ">
              <strong>IGTV</strong>
            </div>
            <div className="tab-item ">
              <strong>บันทึกไว้</strong>
            </div>
            <div className="tab-item ">
              <strong>มีผู้เเท็ก</strong>
            </div>
          </div>

          <div className="tabs_content">
            <div className="tabs_content--item">
              <div className="gallery_grid">
                <div className="grid-img">
                  <div className="grid-img-icon">
                    <span><i className="fas fa-heart"></i>12</span>
                    <span><i className="fas fa-comment"></i>5</span>
                  </div>

                  <img src="https://i2.wp.com/www.shorteng.com/wp-content/uploads/2015/12/photo.jpg?resize=470%2C313" />
                </div>
                <div className="grid-img">
                  <div className="grid-img-icon">
                    <span>
                      <i className="fas fa-heart"></i>12
                    </span>
                    <span>
                      <i className="fas fa-comment"></i>5
                    </span>
                  </div>

                  <img src="https://scontent.fbkk5-8.fna.fbcdn.net/v/t1.0-9/87984302_2656669851231906_8292055902530306048_n.jpg?_nc_cat=106&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGpsnlkv8UPx3-gSZBCOpysTYq01Z2FqKtNirTVnYWoq8eJEaNh274FD-O8S2uFqcc3DOaVKNUYuMLUJsorAWoN&_nc_ohc=Yen3G8xoMM8AX_jRauk&_nc_ht=scontent.fbkk5-8.fna&oh=648c274c3c45714eef2b181c428a667e&oe=5FF311F2" />
                </div>
                <div className="grid-img">
                  <div className="grid-img-icon">
                    <span>
                      <i className="fas fa-heart"></i>12
                    </span>
                    <span>
                      <i className="fas fa-comment"></i>5
                    </span>
                  </div>

                  <img src="https://scontent.fbkk5-4.fna.fbcdn.net/v/t1.0-9/49751008_2322460997986128_5065676169127395328_o.jpg?_nc_cat=110&ccb=2&_nc_sid=174925&_nc_eui2=AeHh5PdNVhhe8qSM838D71iBdleseEGkYD52V6x4QaRgPpSPl4t3RnQwsB9fpHynUADAN3h4lTAWoHvhZ7g8aUfS&_nc_ohc=REnjeFge7ZYAX_4BRGx&_nc_ht=scontent.fbkk5-4.fna&oh=19c968d69f8e04d7c7ef2dadb4c41ee4&oe=5FF557D9" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default App;
